/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 11:19:08 by abinet            #+#    #+#             */
/*   Updated: 2023/09/17 15:39:21 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H

# define PHILO_H

# include <sys/time.h>
# include <string.h>
# include <pthread.h>
# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <limits.h>
# include <stdint.h>
# include <stdbool.h>

# define EATING				"is eating"
# define SLEEPING			"is sleeping"
# define THINKING			"is thinking"
# define DIED				"died"
# define FORK				"has taken a fork"
# define ARG_ERR			"Error! Non valid arguments."
# define ARG_NB_ERR			"Error! Non valid arguments count."
# define ARG_LIMIT_ERR		"Error! Value is out of range."
# define MEAL_ERR			"Error! All philosophers have to eat at least once."
# define MUTEX_ERR			"Error! An issue occured while creating a mutex."
# define MUTEX_DESTROY_ERR	"Error! An issue occured while destroying a mutex."
# define THREAD_ERR			"Error! An issue occured while creating a thread."
# define MALLOC_ERR			"Error! An issue occured while assigning memory."

typedef struct s_philo
{
	int				id;
	int				left_fork_idx;
	int				right_fork_idx;
	unsigned long	last_meal;
	int				meal_count;
	int				hungry;
	pthread_t		thread;
	struct s_data	*data;
}	t_philo;

typedef struct s_data
{
	int				nb_philo;
	int				time_to_die;
	int				time_to_eat;
	int				time_to_sleep;
	int				time_to_think;
	int				tot_meals;
	int				dead_end;
	int				all_ate;
	unsigned long	start;
	pthread_t		thread;
	struct s_philo	philos[200];
	pthread_mutex_t	forks[200];
	pthread_mutex_t	printing;
	pthread_mutex_t	health_check;
	pthread_mutex_t	mut_start;
}	t_data;

// check

int				ft_check_args(int argc, char **argv);
int				ft_check_argv(char *argv);
int				ft_check_health(t_data *philo);
int				ft_check_vitals(t_data *data, int i);

// init

int				ft_init_dinner(t_data *data, char **argv);
int				ft_init_mutex(t_data *data);
int				ft_init_philosophers(t_data *data);
int				init_time_to_think(t_data *data);

// diner

int				ft_start_dinner(t_data *data);
void			ft_is_eating(t_data *data, t_philo *philo);
void			*ft_philo_alone(t_data *data, t_philo *philo);
void			ft_print_state(t_philo *philo, t_data *data, char *state);
void			*ft_threader(void *void_philo);
void			ft_til_the_end(t_data *philosophers, int i, int vitals);

// destroy

int				ft_destroy_mutex(t_data *data, int limit);
int				ft_destroy_thread(t_data *data, int limit);

// time

unsigned long	ft_get_time(void);
unsigned long	ft_time_diff(unsigned long a, unsigned long b);
void			ft_usleep(unsigned long ms);

// utils

void			ft_putstr_fd(char *s, int fd);
int				ft_strcmp(const char *s1, const char *s2);
int				ft_atoi(const char *str);
int				ft_is_num(char *argv);

#endif
