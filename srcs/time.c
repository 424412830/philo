/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/08 20:01:50 by abinet            #+#    #+#             */
/*   Updated: 2023/09/17 16:11:34 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

unsigned long	ft_get_time(void)
{
	struct timeval	time;
	unsigned long	s;
	unsigned long	u;

	gettimeofday(&time, NULL);
	s = time.tv_sec;
	u = time.tv_usec;
	return (s * 1000 + u / 1000);
}

unsigned long	ft_time_diff(unsigned long a, unsigned long b)
{
	if (a < b)
		return (0);
	return (a - b);
}

void	ft_usleep(unsigned long ms)
{
	unsigned long	time;

	time = ft_get_time();
	while (ft_time_diff(ft_get_time(), time) < ms)
		usleep(100);
}
