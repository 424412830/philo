/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dinner.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/08 20:24:40 by abinet            #+#    #+#             */
/*   Updated: 2023/09/17 16:11:01 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	ft_start_dinner(t_data *data)
{
	int				i;

	i = 0;
	pthread_mutex_lock(&(data->mut_start));
	while (i < data->nb_philo)
	{
		if (pthread_create(&(data->philos[i].thread), NULL,
				ft_threader, &(data->philos[i])))
			return (ft_destroy_thread(data, i),
				ft_putstr_fd(THREAD_ERR, STDERR_FILENO), 1);
		i++;
	}
	data->start = ft_get_time();
	while (i >= 0)
	{
		pthread_mutex_lock(&(data->health_check));
		data->philos[i].last_meal = ft_get_time();
		pthread_mutex_unlock(&(data->health_check));
		i--;
	}
	pthread_mutex_unlock(&(data->mut_start));
	ft_til_the_end(data, 0, 0);
	ft_destroy_thread(data, data->nb_philo);
	ft_destroy_mutex(data, data->nb_philo);
	return (0);
}

static void	ft_run_loop(t_data *data, t_philo *philo)
{
	while (1)
	{
		if (ft_check_health(data) == 0)
			break ;
		pthread_mutex_lock(&(data->forks[philo->right_fork_idx]));
		ft_print_state(philo, data, FORK);
		pthread_mutex_lock(&(data->forks[philo->left_fork_idx]));
		ft_print_state(philo, data, FORK);
		ft_is_eating(data, philo);
		pthread_mutex_unlock(&(data->forks[philo->right_fork_idx]));
		pthread_mutex_unlock(&(data->forks[philo->left_fork_idx]));
		ft_print_state(philo, data, SLEEPING);
		ft_usleep(data->time_to_sleep);
		ft_print_state(philo, data, THINKING);
		ft_usleep(data->time_to_think);
	}
}

void	*ft_threader(void *void_philo)
{
	t_philo		*philo;
	t_data		*data;

	philo = (t_philo *)void_philo;
	data = philo->data;
	pthread_mutex_lock(&(data->mut_start));
	pthread_mutex_unlock(&(data->mut_start));
	if (data->nb_philo == 1)
		return (ft_philo_alone(data, philo));
	if (philo->id % 2 == 0)
		ft_usleep(data->time_to_eat);
	ft_run_loop(data, philo);
	return (0);
}

void	ft_til_the_end(t_data *philosophers, int i, int vitals)
{
	int	all_philos;

	all_philos = 0;
	while (1)
	{
		i = 0;
		if (ft_check_health(philosophers) == 0)
			break ;
		while (i < philosophers->nb_philo)
		{
			vitals = ft_check_vitals(philosophers, i);
			if (vitals == 1)
				break ;
			else if (vitals == 2)
				all_philos++;
			if (all_philos == philosophers->nb_philo)
			{
				pthread_mutex_lock(&(philosophers->health_check));
				philosophers->all_ate = 1;
				pthread_mutex_unlock(&(philosophers->health_check));
			}
			i++;
		}
		usleep(100);
	}
}
