/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/08 17:56:21 by abinet            #+#    #+#             */
/*   Updated: 2023/09/17 16:11:30 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	main(int argc, char **argv)
{
	t_data	*data;

	if (ft_check_args(argc, argv))
		return (1);
	data = malloc(sizeof(t_data));
	if (!data)
		return (ft_putstr_fd(MALLOC_ERR, 2), 1);
	if (ft_init_dinner(data, argv))
		return (free(data), 1);
	if (ft_start_dinner(data))
		return (ft_destroy_mutex(data, data->nb_philo), 1);
	return (0);
}
