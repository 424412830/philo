/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dinner_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/12 13:03:41 by abinet            #+#    #+#             */
/*   Updated: 2023/09/17 16:11:05 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	ft_print_state(t_philo *philo, t_data *data, char *state)
{
	pthread_mutex_lock(&(data->printing));
	if (ft_check_health(data) == 0 && state[0] != 'd')
	{
		pthread_mutex_unlock(&(data->printing));
		return ;
	}
	printf("%ld %d %s\n", ft_time_diff(ft_get_time(),
			data->start), philo->id, state);
	pthread_mutex_unlock(&(data->printing));
}

void	ft_is_eating(t_data *data, t_philo *philo)
{
	pthread_mutex_lock(&(data->health_check));
	philo->last_meal = ft_get_time();
	pthread_mutex_unlock(&(data->health_check));
	ft_print_state(philo, data, EATING);
	ft_usleep(data->time_to_eat);
	pthread_mutex_lock(&(data->health_check));
	philo->meal_count++;
	pthread_mutex_unlock(&(data->health_check));
}

void	*ft_philo_alone(t_data *data, t_philo *philo)
{
	pthread_mutex_lock(&(data->forks[philo->right_fork_idx]));
	ft_print_state(philo, data, FORK);
	pthread_mutex_unlock(&(data->forks[philo->right_fork_idx]));
	return (0);
}
