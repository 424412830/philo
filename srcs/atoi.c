/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   atoi.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/17 15:48:59 by abinet            #+#    #+#             */
/*   Updated: 2023/09/17 15:49:28 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philo.h>

static bool	is_whitespace(const char c)
{
	return ((c >= 9 && c <= 13) || c == ' ');
}

static void	skip_whitespaces(const char **str)
{
	while (is_whitespace(**str))
		(*str)++;
}

int	ft_atoi(const char *str)
{
	long int	res;
	int			i;

	res = 0;
	i = 1;
	skip_whitespaces(&str);
	if (*str == '-')
		i = -1;
	if (*str == '-' || *str == '+')
		str++;
	while (*str >= '0' && *str <= '9')
	{
		if ((res > (INT_MAX - *str + '0') / 10))
			return (0);
		res = (res * 10) + *str - '0';
		str++;
	}
	return (res * i);
}
