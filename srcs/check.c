/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/06 11:37:01 by abinet            #+#    #+#             */
/*   Updated: 2023/09/17 16:11:20 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	ft_check_args(int argc, char **argv)
{
	int	i;

	i = 1;
	if (argc < 5 || argc > 6)
	{
		ft_putstr_fd(ARG_NB_ERR, STDERR_FILENO);
		return (1);
	}
	while (i < argc)
	{
		if (!ft_is_num(argv[i]))
		{
			ft_putstr_fd(ARG_ERR, STDERR_FILENO);
			return (1);
		}
		if (!ft_check_argv(argv[i]))
		{
			ft_putstr_fd(ARG_LIMIT_ERR, STDERR_FILENO);
			return (1);
		}
		i++;
	}
	return (0);
}

int	ft_check_argv(char *argv)
{
	if (!ft_atoi(argv))
		return (0);
	return (1);
}

int	ft_check_health(t_data *philo)
{
	int	i;

	pthread_mutex_lock(&(philo->health_check));
	if (philo->all_ate || philo->dead_end)
		i = 0;
	else
		i = 1;
	pthread_mutex_unlock(&(philo->health_check));
	return (i);
}

int	ft_check_vitals(t_data *data, int i)
{
	pthread_mutex_lock(&(data->health_check));
	if (ft_time_diff(ft_get_time(), data->philos[i].last_meal)
		> (unsigned long)data->time_to_die)
	{
		data->dead_end = 1;
		pthread_mutex_unlock(&(data->health_check));
		ft_print_state(&(data->philos[i]), data, DIED);
		return (1);
	}
	else if (data->tot_meals > 0 && data->philos[i].hungry == 1)
	{
		if (data->philos[i].meal_count >= data->tot_meals)
		{
			data->philos[i].hungry = 0;
			pthread_mutex_unlock(&(data->health_check));
			return (2);
		}
		pthread_mutex_unlock(&(data->health_check));
	}
	else
		pthread_mutex_unlock(&(data->health_check));
	return (0);
}
