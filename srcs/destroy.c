/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   destroy.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/08 19:23:59 by abinet            #+#    #+#             */
/*   Updated: 2023/09/17 16:11:16 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	ft_destroy_mutex(t_data *data, int limit)
{
	int	i;

	i = 0;
	while (i < limit)
	{
		if (pthread_mutex_destroy(&(data->forks[i])) != 0)
			ft_putstr_fd(MUTEX_DESTROY_ERR, STDERR_FILENO);
		i++;
	}
	if (limit == data->nb_philo)
	{
		if (pthread_mutex_destroy(&(data->printing)) != 0)
			ft_putstr_fd(MUTEX_DESTROY_ERR, STDERR_FILENO);
		if (pthread_mutex_destroy(&(data->health_check)) != 0)
			ft_putstr_fd(MUTEX_DESTROY_ERR, STDERR_FILENO);
	}
	free(data);
	return (0);
}

int	ft_destroy_thread(t_data *data, int limit)
{
	int	i;

	i = 0;
	while (i < limit)
	{
		pthread_join(data->philos[i].thread, NULL);
		i++;
	}
	return (0);
}
