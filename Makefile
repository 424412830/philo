NAME		= philo

################## FILES & DIRECTORY ###################################################

SRCS_FILES	+= main.c
SRCS_FILES	+= utils.c
SRCS_FILES	+= check.c
SRCS_FILES	+= init.c
SRCS_FILES	+= destroy.c
SRCS_FILES	+= dinner.c
SRCS_FILES	+= time.c
SRCS_FILES	+= dinner_2.c
SRCS_FILES	+= atoi.c
SRCS_DIR	= srcs/
SRCS		= $(addprefix $(SRCS_DIR),$(SRCS_FILES))


OBJS_FILES	= $(SRCS:.c=.o)
OBJS_DIR	= objs/
#OBJS		= $(addprefix $(OBJS_DIR),$(OBJS_FILES))

HEADER_DIR		= include/
HEADER_FILE		= philo.h
HEADER			= $(addprefix ${HEADER_DIR}, ${HEADER_FILE})
HEADERS_DIR		= -I $(HEADER_DIR)

########################################################################################

CC			= cc

FLAGS		= -pthread -Wall -Wextra -Werror -g3


###################### RULES ##########################################################


all:	${NAME}

${NAME}:	${OBJS_FILES} ${HEADER}
		${CC} ${OBJS_FILES} -o ${NAME} ${HEADERS_DIR}
		mkdir -p ${OBJS_DIR}
		mv ${OBJS_FILES} ${OBJS_DIR}

%.o: %.c
		${CC} ${FLAGS} -c $< -o ${<:.c=.o} ${HEADERS_DIR}

clean :
		${RM} -r ${OBJS_DIR}

fclean : clean
		${RM} -r ${NAME}

re : fclean
	$(MAKE)

.SECONDARY : $(OBJS_FILES)

.PHONY :
